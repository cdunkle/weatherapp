package cdunkle;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class Weather {
    //Our zip code, we don't want people to access this by themselves so we make it a private variable
     private String zip;

     JsonElement jse;

     public Weather(String k)
     {
         zip = k;
     }

     //This will take our input and "fetch" the information back, but won't be displaying it
    public void fetch()
    {
        try {
            String urlString = "https://api.aerisapi.com/observations/"
                    + URLEncoder.encode(zip, "utf-8")
                    + "?client_id=CF0efkpGXmI6VjS5CP7wO&client_secret=qQqWhVMgh9MJNkSjDk0XTwkc25Ps9TrwNz6wSGHN";

            URL weatherURL = new URL(urlString);

            InputStream is = weatherURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            jse = JsonParser.parseReader(br);

        } catch (UnsupportedEncodingException | MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //This will retrieve our temperature in fahrenheit
    public double getTemp()
    {
        if (jse.getAsJsonObject().get("success").toString().equals("true")) {
            return jse.getAsJsonObject().get("response").getAsJsonObject().get("ob").getAsJsonObject().get("tempF").getAsDouble();
        }
        else
        {
            return (-999);
        }
    }

    //This will retrieve our temperature in celsius
    public double getCTemp() {
        if (jse.getAsJsonObject().get("success").toString().equals("true"))
        {
            return jse.getAsJsonObject().get("response").getAsJsonObject().get("ob").getAsJsonObject().get("tempC").getAsDouble();
        }
        else
        {
            return (-999);
        }
    }

    //This method will return the location from the API
    public String getCityState()
    {
        if (jse.getAsJsonObject().get("success").toString().equals("true"))
        {
            return jse.getAsJsonObject().get("response").getAsJsonObject().get("place").getAsJsonObject().get("city").getAsString()
                    + ", "
                    +jse.getAsJsonObject().get("response").getAsJsonObject().get("place").getAsJsonObject().get("state").getAsString().toUpperCase();

        }
        else
        {
            return "Invalid Zip";
        }

    }

    //This method will return the weather from the API for us to use in our controller class
    public String getCurrentWeather()
    {
        if (jse.getAsJsonObject().get("success").toString().equals("true"))
        {
            return jse.getAsJsonObject().get("response").getAsJsonObject().get("ob").getAsJsonObject().get("weather").getAsString();
        }
        else
        {
            return "Invalid Zip";
        }
    }

    //This method will retrieve the icon string from the API for us to use in our controller class
    public String getImage()
    {
        if (jse.getAsJsonObject().get("success").toString().equals("true"))
        {
            return jse.getAsJsonObject().get("response").getAsJsonObject().get("ob").getAsJsonObject().get("icon").getAsString();
        }
        else
        {
            return "null";
        }
    }


//this is our test to make sure that the class is running correctly
    /*
    public static void main(String[] args)
    {
        Weather r = new Weather("95677");
        r.fetch();
        System.out.println(r.getTemp());


    }
    */
}