package cdunkle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class Controller
{
    int tempChange = 1;


    @FXML
    TextField zipField;

    @FXML
    Label tempLabel;

    @FXML
    Label weatherLabel;

    @FXML
    Label cityStateLabel;

    @FXML
    Button tempBtn;

    @FXML
    ImageView iconImage;


    public void handleFetchButton(ActionEvent e)
    {
        //Constructs weather object for us
        String zip = zipField.getText();
        Weather w = new Weather(zip);
        w.fetch();

        //Button Control
        tempBtn.setText("F");

        //Image Control
        Image img = new Image("file:images/" + w.getImage());
        iconImage.setImage(img);

        //Temperature Label
        tempLabel.setText("" + w.getTemp() + " F");

        //Location Label
        cityStateLabel.setText(w.getCityState());

        //Current Weather Label
        weatherLabel.setText(w.getCurrentWeather());
    }

    //This event handles the switching of temperatures from fahrenheit and celsius
    public void handleFetchButton2(ActionEvent e)
    {
        tempChange *= -1;
        String zip = zipField.getText();
        Weather w = new Weather(zip);
        w.fetch();
        //Fahrenheit
        if (tempChange == 1)
        {
            tempBtn.setText("F");
            tempLabel.setText("" + w.getTemp() + " F");
        }
        //Celsius
        else
        {
            tempBtn.setText("C");
            tempLabel.setText("" + w.getCTemp() + " C");
        }

    }



}
